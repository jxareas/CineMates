package com.example.cinemates.di.component

import dagger.hilt.android.HiltAndroidApp
import android.app.Application

/**
 * @author Antonio Di Nuzzo
 * Created 21/04/2022 at 15:28
 */
@HiltAndroidApp
class BaseApplication : Application()